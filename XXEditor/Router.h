#ifndef _EDITOR_ROTER_H_
#define _EDITOR_ROTER_H_

#include <vector>
#include "qobject.h"
#include "Singleton.h"

class TreeItem;
class QVariant;
class QString;
class QMouseEvent;

class Router : public QObject, public Singleton<Router>
{
    Q_OBJECT

public:
    Router() 
        : _ignoreValueChange(false)
    {
    }

public slots:
    void onAddItem(TreeItem* item, const QString& name, const QString& type);
    void onSelectedItem(TreeItem* item);
    void onValueChange(TreeItem* item, const QString& name, const QString& value);

signals:

    void onAddProp(const char* name, 
        const char* type, const char* value, const char* choices, bool readonly);
    void onSetProp(const char* name, const char* value);
    void onClearProp();

public:

    //for lua
    void addProp(const char* name, 
        const char* type, const char* value, const char* choices, bool readonly);
    void setProp(const char* name, const char* value);
    void clearProp();
    void throwError(const char* msg);

    void addType(const char* name) { _alltype.push_back(name); }
    const std::vector<QString>& getAllType() { return _alltype; }

private:

    bool _ignoreValueChange;

    std::vector<QString> _alltype;
};


#endif//_EDITOR_ROTER_H_