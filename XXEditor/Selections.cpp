#include "Selections.h"

SINGLETON_INSTANCE(Selection);

void Selection::add(Widget* w)
{
    if (w)
        _selected.insert(w);
    emit onSelectedChange();
}

void Selection::remove(Widget* w)
{
    _selected.erase(w);
}

const std::set<Widget*>& Selection::get()
{
    return _selected;
}

void Selection::clear()
{
    _selected.clear();
    emit onSelectedChange();
}

void Selection::selected(Widget* w)
{
    _selected.clear();
    add(w);
}

int Selection::size()
{
    return _selected.size();
}

Widget* Selection::getCurrent()
{
    if (size() == 0)
        return nullptr;
    return *_selected.begin();
}
