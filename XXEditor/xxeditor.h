#ifndef XXEDITOR_H
#define XXEDITOR_H

#include <QtWidgets/QMainWindow>
#include "ui_xxeditor.h"
#include "XXUI_fix.h"


class QCGLWidget;
class QtProperty;
class QVariant;
class XXPrivate;
class BoxList;
class Router;
class EditorScene;
class TreeItem;

class XXEditor : public QMainWindow
{
    Q_OBJECT

public:
    XXEditor(QWidget *parent = 0);
    ~XXEditor();

    void createActions();
    void createProp();
    void createGL();
    void createTree();

    void setFactory(const QString& name);


private slots:
    void setStyle(QAction*);
    void save();
    void saveCode();
    void open();
    void reload();

    void newFile();
    void loadCC();
    void currentChanged();

    void onDeleteItem(TreeItem*);

    void setBgColor();
    void setWorkPath();
    void setBoxShow(bool);

    void onSetPath(const QString& w, const QString& s);

    void valueChanged(QtProperty *property, double value);
    void valueChanged(QtProperty *property, const QString &value);
    void valueChanged(QtProperty *property, const QColor &value);
    void valueChanged(QtProperty *property, const QPointF &value);
    void valueChanged(QtProperty *property, const QSizeF &value);
    void valueChanged(QtProperty *property, const QRectF &value);
    void valueChanged(QtProperty *property, bool value);
    void valueChanged(QtProperty *property, int value);

    void onLoadFileButtonClick(QtProperty *);

    void clearProp();
    void setProp(const char* name, const char* value);
    void addProp(const char* name, const char* type, const char* value, const char* choices, bool readyonly);
    void updateValue(TreeItem* item);
    void updateProp(const QString&);

public:
    void setCurrentFile(const QString& file);

protected:
    virtual void closeEvent(QCloseEvent *event);
    void readSettings();
private:
    QWidget* _widgetRoot;
    BoxList* _boxList;
    QColor _bgColor;

    QString _workPath;
    QString _scripsPath;

    QString _currentSaveFile;

    XXPrivate* _d;
    EditorScene* _gl;
    Ui::XXEditorClass ui;
};

#endif // XXEDITOR_H
