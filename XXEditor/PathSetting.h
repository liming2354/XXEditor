#ifndef PATHSETTING_H
#define PATHSETTING_H

#include <QWidget>
#include "ui_PathSetting.h"
#include <functional>



class PathSetting : public QWidget
{
    Q_OBJECT

public:
    PathSetting(QWidget *parent = 0);
    ~PathSetting();

    void set(const QString& w, const QString& s);

public slots:
    void browse2();
    void browse();
    void ok();
    void cancel();

signals:

    void onSet(const QString& w, const QString& s);

private:

    bool _changed;
    Ui::win ui;
};

#endif // PATHSETTING_H
