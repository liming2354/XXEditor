#ifndef __SIGNLETON_H__
#define __SIGNLETON_H__

#include <assert.h>

template <typename T>
class Singleton
{
public:
    Singleton()
    {
        assert(!_instance);
        _instance = static_cast<T*>(this);
    }
    virtual ~Singleton()
    {
        _instance = nullptr;
    }

    static T& getInstance()
    { 
        return *_instance;
    }
    static T* getInstancePtr()
    {
        return _instance;
    }

protected:
    static T* _instance;
};

#define SINGLETON_INSTANCE(type) \
    template <> type* Singleton<type>::_instance = nullptr;

#endif//__SIGNLETON_H__