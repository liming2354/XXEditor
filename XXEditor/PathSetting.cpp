#include "PathSetting.h"
#include "qfiledialog.h"

PathSetting::PathSetting(QWidget *parent)
    : QWidget(parent)
{
    ui.setupUi(this);

    connect(ui.browse_btn, SIGNAL(released()), this, SLOT(browse()));
    connect(ui.ok, SIGNAL(released()), this, SLOT(ok()));
    connect(ui.cancel, SIGNAL(released()), this, SLOT(cancel()));

    connect(ui.browse2, SIGNAL(released()), this, SLOT(browse2()));

}

PathSetting::~PathSetting()
{

}

void PathSetting::cancel()
{
    this->close();
}

void PathSetting::browse2()
{
    QString path = QFileDialog::getExistingDirectory(this, tr("Choose a File"), "");

    if (path.isNull() || path.isEmpty())
        return;

    ui.lineEdit_2->setText(path);
}

void PathSetting::browse()
{
    QString path = QFileDialog::getExistingDirectory(this, tr("Choose a File"), "");

    if (path.isNull() || path.isEmpty())
        return;

    ui.lineEdit->setText(path);
}

void PathSetting::ok()
{
    emit onSet(ui.lineEdit->text(), ui.lineEdit_2->text());

    this->close();
}

void PathSetting::set(const QString& w, const QString& s)
{
    ui.lineEdit->setText(w);
    ui.lineEdit_2->setText(s);
}
