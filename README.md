
#XXEditor

**XXEditor**是一个cocos2d-x 2.x的UI编辑器，内部使用CocosStudio控件。使用[Qt5.4.1](http://www.qt.io/) + [quick-cocos2d-x](https://github.com/CaptainCN/quick-cocos2d-x) 开发，可运行在Windows、Mac、Linux。这个项目是[QCEditor](https://github.com/CaptainCN/QCEditor)的2.x lua版本。希望能帮助那些还在2.x奋斗的同学。

###功能列表:
* 只保存修改过的属性，提升加载速度。
* 使用Lua Table 作为配置文件，并且解析代码全为Lua（2个文件）。
* 可使用lua 携程加载。感觉和多线程一样，界面不卡顿。
* 任意一个控件可单独存为文件，任意一个界面文件可以作为子控件加入到当前界面。
* 能够加载Cocostudio的Json/Jsb等文件。（整图pist需要预先拆开）
* 可导出Lua/C++ 代码
* 支持lua自定义控件
* 简单的Undo、Redo
* 跨平台
* 协议MIT



**XXEditor** is a UI Editor for cocos2d-x/quick-cocos2d-x. Created by [Qt5.4.1](http://www.qt.io/) + [quick-cocos2d-x](https://github.com/CaptainCN/quick-cocos2d-x).Is new version of [QCEditor](https://github.com/CaptainCN/QCEditor).

### Features:
* Just save changed attributes.
* Save as lua table. And using lua read it.
* Can using Lua coroutine load file. Like mult thread.
* Any Widget can save as a file. Any file can load as child.
* Load Cocostudio Json/Jsb files.(split sprite sheet plist first.)
* Export c++/lua code.
* Custom Widgets using lua.
* Undo/Redo.
* Cross platform.
* MIT

##Build windows.
* VS2013 + Qt 5.4.1 + QtCreator
* Put [quick-cocos2d-x](https://github.com/CaptainCN/quick-cocos2d-x) in XXEditor/Quick/quick-cocos2d-x
* QtCreator open `qteditor\qtpropertybrowser\buildlib\buildlib.pro` and Build
* Open XXEditor.sln build.

##Build mac.
* CMake + Qt 5.4.1 + QtCreator
* Put [quick-cocos2d-x](https://github.com/CaptainCN/quick-cocos2d-x) in XXEditor/Quick/quick-cocos2d-x
* QtCreator open CMakeLists.txt build.

![preview][]
![code][]
![save][]

[preview]: preview/preview.jpg
[save]: preview/save.png
[code]: preview/2.jpg