#ifndef _COMMAND_H_
#define _COMMAND_H_

#include "XXUI_fix.h"
#include "QUndoCommand"
#include "qundostack.h"
#include "Singleton.h"


class cmd_mgr;

class cmd_Prop : public QUndoCommand
{
public:
    cmd_Prop(Widget* item, const QString& name, const QString& value);

    virtual void redo();
    virtual void undo();

protected:

    Widget* _widget;
    QString _name;
    QString _undo_value;
    QString _redo_value;

    friend class cmd_mgr;
};

class cmd_Drag : public QUndoCommand
{
public:
    cmd_Drag(std::map<Widget*, std::pair<CCPoint, CCPoint>>&);

    virtual void redo();
    virtual void undo();

protected:
    std::map<Widget*, std::pair<CCPoint, CCPoint>> _map;
    friend class cmd_mgr;
};

class cmd_mgr
    : public QUndoStack
    , public Singleton<cmd_mgr>
{
    Q_OBJECT
public:
    cmd_mgr();

    void add(QUndoCommand *cmd);

public slots:
    void undo_();
    void redo_();

protected:
    void startMacro_(const QString& name);
    void stopMacro_();

    bool _ismac;
    QUndoCommand* _last;
};

#endif//_COMMAND_H_