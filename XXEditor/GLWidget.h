#pragma once

#include <QtOpenGL/QGLWidget>

class QTimer;

class QCGLWidget : public QGLWidget
{
    Q_OBJECT

public:
	QCGLWidget(QWidget* parent = nullptr);
    ~QCGLWidget();

    void createCocos();
    void InitGL();

public slots:
	void cocos2dDraw();

protected:
    void _doDraw();

	virtual void paintEvent(QPaintEvent* event);
	virtual void resizeEvent(QResizeEvent* evnet);

	QTimer* _timer;
};

