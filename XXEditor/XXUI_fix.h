#ifndef __XXIMAGEVIEW_H__
#define __XXIMAGEVIEW_H__

#include "UIWidgets/UIImageView.h"
#include "UIWidgets/UILoadingBar.h"
#include "UIWidgets/UILabelBMFont.h"
#include "UIWidgets/UIButton.h"
#include "UIWidgets/UICheckBox.h"
#include "UIWidgets/UISlider.h"
#include "UIWidgets/UILabelAtlas.h"
#include "UIWidgets/UILabel.h"
#include "Layouts/UILayout.h"
#include "BaseClasses/UIWidget.h"
#include "UIWidgets/UITextField.h"
#include "UIWidgets/ScrollWidget/UIListView.h"
#include "UIWidgets/ScrollWidget/UIScrollView.h"
#include "UIWidgets/ScrollWidget/UIPageView.h"

USING_NS_CC;
using namespace ui;

#define Impl(className) \
class XX##className : public className \
{ \
public:\
    CREATE_FUNC(XX##className);  \
    virtual void draw(); \
    virtual Widget* createCloneInstance() { return XX##className::create(); } \
};

Impl(Layout)
Impl(ScrollView)
Impl(ListView)
Impl(PageView)
Impl(Widget)
Impl(Label)
Impl(LabelAtlas)
Impl(ImageView)
Impl(LoadingBar)
Impl(LabelBMFont)
Impl(TextField)
Impl(Slider)
Impl(Button)
Impl(CheckBox)

#endif//__XXIMAGEVIEW_H__