
#ifndef _SELECTIONS_H_
#define _SELECTIONS_H_

#include "XXUI_fix.h"
#include "Singleton.h"
#include "qobject.h"
#include <set>

class Selection 
    : public QObject
    , public Singleton<Selection>
{
    Q_OBJECT

public:
    void add(Widget* w);
    void remove(Widget* w);
    void clear();

    //clear and add
    void selected(Widget* w);
    int size();

    Widget* getCurrent();

    const std::set<Widget*>& get();

signals:
    void onSelectedChange();

protected:
    std::set<Widget*> _selected;
};

#endif // !_SELECTIONS_H_