/*
** Lua binding: XXUI
** Generated automatically by tolua++-1.0.92 on 12/04/15 15:04:59.
*/

/****************************************************************************
 Copyright (c) 2011 cocos2d-x.org

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

extern "C" {
#include "tolua_fix.h"
}

#include <map>
#include <string>
#include "cocos2d.h"
#include "CCLuaEngine.h"
#include "SimpleAudioEngine.h"
#include "cocos-ext.h"
#include "UIWidgets/UIButton.h"

using namespace cocos2d;
using namespace cocos2d::ui;
using namespace cocos2d::extension;
using namespace CocosDenshion;

/* Exported function */
TOLUA_API int  tolua_XXUI_open (lua_State* tolua_S);

#include "XXUI_fix.h"

/* function to release collected object via destructor */
#ifdef __cplusplus




#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"XXScrollView");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXScrollView)), "XXScrollView");
 tolua_usertype(tolua_S,"TextField");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(TextField)), "TextField");
 tolua_usertype(tolua_S,"Label");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(Label)), "Label");
 tolua_usertype(tolua_S,"ListView");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(ListView)), "ListView");
 tolua_usertype(tolua_S,"XXLabelBMFont");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXLabelBMFont)), "XXLabelBMFont");
 tolua_usertype(tolua_S,"XXCheckBox");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXCheckBox)), "XXCheckBox");
 tolua_usertype(tolua_S,"ScrollView");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(ScrollView)), "ScrollView");
 tolua_usertype(tolua_S,"XXListView");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXListView)), "XXListView");
 tolua_usertype(tolua_S,"LabelAtlas");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(LabelAtlas)), "LabelAtlas");
 tolua_usertype(tolua_S,"XXSlider");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXSlider)), "XXSlider");
 tolua_usertype(tolua_S,"Button");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(Button)), "Button");
 tolua_usertype(tolua_S,"PageView");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(PageView)), "PageView");
 tolua_usertype(tolua_S,"XXImageView");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXImageView)), "XXImageView");
 tolua_usertype(tolua_S,"XXPageView");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXPageView)), "XXPageView");
 tolua_usertype(tolua_S,"XXLayout");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXLayout)), "XXLayout");
 tolua_usertype(tolua_S,"ImageView");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(ImageView)), "ImageView");
 tolua_usertype(tolua_S,"Widget");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(Widget)), "Widget");
 tolua_usertype(tolua_S,"XXLabel");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXLabel)), "XXLabel");
 tolua_usertype(tolua_S,"Layout");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(Layout)), "Layout");
 tolua_usertype(tolua_S,"CheckBox");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(CheckBox)), "CheckBox");
 tolua_usertype(tolua_S,"XXWidget");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXWidget)), "XXWidget");
 tolua_usertype(tolua_S,"XXButton");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXButton)), "XXButton");
 tolua_usertype(tolua_S,"LoadingBar");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(LoadingBar)), "LoadingBar");
 tolua_usertype(tolua_S,"XXTextField");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXTextField)), "XXTextField");
 tolua_usertype(tolua_S,"Slider");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(Slider)), "Slider");
 tolua_usertype(tolua_S,"XXLoadingBar");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXLoadingBar)), "XXLoadingBar");
 tolua_usertype(tolua_S,"CCRect");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(CCRect)), "CCRect");
 tolua_usertype(tolua_S,"XXLabelAtlas");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(XXLabelAtlas)), "XXLabelAtlas");
 tolua_usertype(tolua_S,"LabelBMFont");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(LabelBMFont)), "LabelBMFont");
}

/* method: create of class  XXImageView */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXImageView_create00
static int tolua_XXUI_XXImageView_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXImageView",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXImageView* tolua_ret = (XXImageView*)  XXImageView::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXImageView");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: isScale9Enabled of class  XXImageView */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXImageView_isScale9Enabled00
static int tolua_XXUI_XXImageView_isScale9Enabled00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXImageView",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXImageView* self = (XXImageView*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'isScale9Enabled'", NULL);
#endif
  {
   bool tolua_ret = (bool)  self->isScale9Enabled();
   tolua_pushboolean(tolua_S,(bool)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'isScale9Enabled'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTexture of class  XXImageView */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXImageView_getTexture00
static int tolua_XXUI_XXImageView_getTexture00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXImageView",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXImageView* self = (XXImageView*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTexture'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getTexture();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTexture'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getCapInsets of class  XXImageView */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXImageView_getCapInsets00
static int tolua_XXUI_XXImageView_getCapInsets00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXImageView",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXImageView* self = (XXImageView*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCapInsets'", NULL);
#endif
  {
   const CCRect& tolua_ret = (const CCRect&)  self->getCapInsets();
    tolua_pushusertype(tolua_S,(void*)&tolua_ret,"const CCRect");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCapInsets'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXLoadingBar */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXLoadingBar_create00
static int tolua_XXUI_XXLoadingBar_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXLoadingBar",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXLoadingBar* tolua_ret = (XXLoadingBar*)  XXLoadingBar::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXLoadingBar");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: isScale9Enabled of class  XXLoadingBar */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXLoadingBar_isScale9Enabled00
static int tolua_XXUI_XXLoadingBar_isScale9Enabled00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXLoadingBar",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXLoadingBar* self = (XXLoadingBar*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'isScale9Enabled'", NULL);
#endif
  {
   bool tolua_ret = (bool)  self->isScale9Enabled();
   tolua_pushboolean(tolua_S,(bool)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'isScale9Enabled'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTexture of class  XXLoadingBar */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXLoadingBar_getTexture00
static int tolua_XXUI_XXLoadingBar_getTexture00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXLoadingBar",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXLoadingBar* self = (XXLoadingBar*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTexture'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getTexture();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTexture'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getCapInsets of class  XXLoadingBar */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXLoadingBar_getCapInsets00
static int tolua_XXUI_XXLoadingBar_getCapInsets00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXLoadingBar",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXLoadingBar* self = (XXLoadingBar*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCapInsets'", NULL);
#endif
  {
   const CCRect& tolua_ret = (const CCRect&)  self->getCapInsets();
    tolua_pushusertype(tolua_S,(void*)&tolua_ret,"const CCRect");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCapInsets'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXLabelBMFont */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXLabelBMFont_create00
static int tolua_XXUI_XXLabelBMFont_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXLabelBMFont",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXLabelBMFont* tolua_ret = (XXLabelBMFont*)  XXLabelBMFont::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXLabelBMFont");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getFntFile of class  XXLabelBMFont */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXLabelBMFont_getFntFile00
static int tolua_XXUI_XXLabelBMFont_getFntFile00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXLabelBMFont",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXLabelBMFont* self = (XXLabelBMFont*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getFntFile'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getFntFile();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getFntFile'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXButton */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXButton_create00
static int tolua_XXUI_XXButton_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXButton",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXButton* tolua_ret = (XXButton*)  XXButton::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXButton");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: isScale9Enabled of class  XXButton */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXButton_isScale9Enabled00
static int tolua_XXUI_XXButton_isScale9Enabled00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXButton",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXButton* self = (XXButton*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'isScale9Enabled'", NULL);
#endif
  {
   bool tolua_ret = (bool)  self->isScale9Enabled();
   tolua_pushboolean(tolua_S,(bool)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'isScale9Enabled'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTextureNormal of class  XXButton */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXButton_getTextureNormal00
static int tolua_XXUI_XXButton_getTextureNormal00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXButton",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXButton* self = (XXButton*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextureNormal'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getTextureNormal();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextureNormal'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTexturePressed of class  XXButton */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXButton_getTexturePressed00
static int tolua_XXUI_XXButton_getTexturePressed00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXButton",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXButton* self = (XXButton*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTexturePressed'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getTexturePressed();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTexturePressed'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTextureDisabled of class  XXButton */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXButton_getTextureDisabled00
static int tolua_XXUI_XXButton_getTextureDisabled00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXButton",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXButton* self = (XXButton*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextureDisabled'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getTextureDisabled();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextureDisabled'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXCheckBox */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXCheckBox_create00
static int tolua_XXUI_XXCheckBox_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXCheckBox",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXCheckBox* tolua_ret = (XXCheckBox*)  XXCheckBox::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXCheckBox");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTextureBackGround of class  XXCheckBox */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXCheckBox_getTextureBackGround00
static int tolua_XXUI_XXCheckBox_getTextureBackGround00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXCheckBox",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXCheckBox* self = (XXCheckBox*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextureBackGround'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getTextureBackGround();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextureBackGround'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTextureBackGroundSelected of class  XXCheckBox */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXCheckBox_getTextureBackGroundSelected00
static int tolua_XXUI_XXCheckBox_getTextureBackGroundSelected00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXCheckBox",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXCheckBox* self = (XXCheckBox*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextureBackGroundSelected'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getTextureBackGroundSelected();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextureBackGroundSelected'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTextureFrontCross of class  XXCheckBox */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXCheckBox_getTextureFrontCross00
static int tolua_XXUI_XXCheckBox_getTextureFrontCross00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXCheckBox",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXCheckBox* self = (XXCheckBox*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextureFrontCross'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getTextureFrontCross();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextureFrontCross'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTextureBackGroundDisabled of class  XXCheckBox */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXCheckBox_getTextureBackGroundDisabled00
static int tolua_XXUI_XXCheckBox_getTextureBackGroundDisabled00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXCheckBox",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXCheckBox* self = (XXCheckBox*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextureBackGroundDisabled'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getTextureBackGroundDisabled();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextureBackGroundDisabled'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTextureFrontCrossDisabled of class  XXCheckBox */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXCheckBox_getTextureFrontCrossDisabled00
static int tolua_XXUI_XXCheckBox_getTextureFrontCrossDisabled00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXCheckBox",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXCheckBox* self = (XXCheckBox*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextureFrontCrossDisabled'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getTextureFrontCrossDisabled();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextureFrontCrossDisabled'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXSlider */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXSlider_create00
static int tolua_XXUI_XXSlider_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXSlider",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXSlider* tolua_ret = (XXSlider*)  XXSlider::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXSlider");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTexture of class  XXSlider */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXSlider_getTexture00
static int tolua_XXUI_XXSlider_getTexture00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXSlider",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXSlider* self = (XXSlider*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTexture'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getTexture();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTexture'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getProgressBarTexture of class  XXSlider */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXSlider_getProgressBarTexture00
static int tolua_XXUI_XXSlider_getProgressBarTexture00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXSlider",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXSlider* self = (XXSlider*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getProgressBarTexture'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getProgressBarTexture();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getProgressBarTexture'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getSlidBallNormalTexture of class  XXSlider */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXSlider_getSlidBallNormalTexture00
static int tolua_XXUI_XXSlider_getSlidBallNormalTexture00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXSlider",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXSlider* self = (XXSlider*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getSlidBallNormalTexture'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getSlidBallNormalTexture();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getSlidBallNormalTexture'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getSlidBallPressedTexture of class  XXSlider */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXSlider_getSlidBallPressedTexture00
static int tolua_XXUI_XXSlider_getSlidBallPressedTexture00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXSlider",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXSlider* self = (XXSlider*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getSlidBallPressedTexture'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getSlidBallPressedTexture();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getSlidBallPressedTexture'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getSlidBallDisabledTexture of class  XXSlider */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXSlider_getSlidBallDisabledTexture00
static int tolua_XXUI_XXSlider_getSlidBallDisabledTexture00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXSlider",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXSlider* self = (XXSlider*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getSlidBallDisabledTexture'", NULL);
#endif
  {
   const std::string tolua_ret = (const std::string)  self->getSlidBallDisabledTexture();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getSlidBallDisabledTexture'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXLabelAtlas */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXLabelAtlas_create00
static int tolua_XXUI_XXLabelAtlas_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXLabelAtlas",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXLabelAtlas* tolua_ret = (XXLabelAtlas*)  XXLabelAtlas::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXLabelAtlas");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXLabel */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXLabel_create00
static int tolua_XXUI_XXLabel_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXLabel",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXLabel* tolua_ret = (XXLabel*)  XXLabel::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXLabel");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getBackGroundImage of class  XXLayout */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXLayout_getBackGroundImage00
static int tolua_XXUI_XXLayout_getBackGroundImage00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXLayout",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXLayout* self = (XXLayout*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getBackGroundImage'", NULL);
#endif
  {
   std::string tolua_ret = (std::string)  self->getBackGroundImage();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getBackGroundImage'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXLayout */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXLayout_create00
static int tolua_XXUI_XXLayout_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXLayout",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXLayout* tolua_ret = (XXLayout*)  XXLayout::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXLayout");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXWidget */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXWidget_create00
static int tolua_XXUI_XXWidget_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXWidget",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXWidget* tolua_ret = (XXWidget*)  XXWidget::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXWidget");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXTextField */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXTextField_create00
static int tolua_XXUI_XXTextField_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXTextField",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXTextField* tolua_ret = (XXTextField*)  XXTextField::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXTextField");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTextHorizontalAlignment of class  XXTextField */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXTextField_getTextHorizontalAlignment00
static int tolua_XXUI_XXTextField_getTextHorizontalAlignment00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXTextField",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXTextField* self = (XXTextField*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextHorizontalAlignment'", NULL);
#endif
  {
   CCTextAlignment tolua_ret = (CCTextAlignment)  self->getTextHorizontalAlignment();
   tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextHorizontalAlignment'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getTextVerticalAlignment of class  XXTextField */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXTextField_getTextVerticalAlignment00
static int tolua_XXUI_XXTextField_getTextVerticalAlignment00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXTextField",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXTextField* self = (XXTextField*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextVerticalAlignment'", NULL);
#endif
  {
   CCVerticalTextAlignment tolua_ret = (CCVerticalTextAlignment)  self->getTextVerticalAlignment();
   tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextVerticalAlignment'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getBackGroundImage of class  XXScrollView */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXScrollView_getBackGroundImage00
static int tolua_XXUI_XXScrollView_getBackGroundImage00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXScrollView",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXScrollView* self = (XXScrollView*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getBackGroundImage'", NULL);
#endif
  {
   std::string tolua_ret = (std::string)  self->getBackGroundImage();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getBackGroundImage'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXScrollView */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXScrollView_create00
static int tolua_XXUI_XXScrollView_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXScrollView",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXScrollView* tolua_ret = (XXScrollView*)  XXScrollView::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXScrollView");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getBackGroundImage of class  XXListView */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXListView_getBackGroundImage00
static int tolua_XXUI_XXListView_getBackGroundImage00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXListView",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXListView* self = (XXListView*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getBackGroundImage'", NULL);
#endif
  {
   std::string tolua_ret = (std::string)  self->getBackGroundImage();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getBackGroundImage'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXListView */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXListView_create00
static int tolua_XXUI_XXListView_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXListView",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXListView* tolua_ret = (XXListView*)  XXListView::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXListView");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getBackGroundImage of class  XXPageView */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXPageView_getBackGroundImage00
static int tolua_XXUI_XXPageView_getBackGroundImage00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"XXPageView",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  XXPageView* self = (XXPageView*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getBackGroundImage'", NULL);
#endif
  {
   std::string tolua_ret = (std::string)  self->getBackGroundImage();
   tolua_pushcppstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getBackGroundImage'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  XXPageView */
#ifndef TOLUA_DISABLE_tolua_XXUI_XXPageView_create00
static int tolua_XXUI_XXPageView_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"XXPageView",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   XXPageView* tolua_ret = (XXPageView*)  XXPageView::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"XXPageView");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* Open function */
TOLUA_API int tolua_XXUI_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
  tolua_cclass(tolua_S,"XXImageView","XXImageView","ImageView",NULL);
  tolua_beginmodule(tolua_S,"XXImageView");
   tolua_function(tolua_S,"create",tolua_XXUI_XXImageView_create00);
   tolua_function(tolua_S,"isScale9Enabled",tolua_XXUI_XXImageView_isScale9Enabled00);
   tolua_function(tolua_S,"getTexture",tolua_XXUI_XXImageView_getTexture00);
   tolua_function(tolua_S,"getCapInsets",tolua_XXUI_XXImageView_getCapInsets00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXLoadingBar","XXLoadingBar","LoadingBar",NULL);
  tolua_beginmodule(tolua_S,"XXLoadingBar");
   tolua_function(tolua_S,"create",tolua_XXUI_XXLoadingBar_create00);
   tolua_function(tolua_S,"isScale9Enabled",tolua_XXUI_XXLoadingBar_isScale9Enabled00);
   tolua_function(tolua_S,"getTexture",tolua_XXUI_XXLoadingBar_getTexture00);
   tolua_function(tolua_S,"getCapInsets",tolua_XXUI_XXLoadingBar_getCapInsets00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXLabelBMFont","XXLabelBMFont","LabelBMFont",NULL);
  tolua_beginmodule(tolua_S,"XXLabelBMFont");
   tolua_function(tolua_S,"create",tolua_XXUI_XXLabelBMFont_create00);
   tolua_function(tolua_S,"getFntFile",tolua_XXUI_XXLabelBMFont_getFntFile00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXButton","XXButton","Button",NULL);
  tolua_beginmodule(tolua_S,"XXButton");
   tolua_function(tolua_S,"create",tolua_XXUI_XXButton_create00);
   tolua_function(tolua_S,"isScale9Enabled",tolua_XXUI_XXButton_isScale9Enabled00);
   tolua_function(tolua_S,"getTextureNormal",tolua_XXUI_XXButton_getTextureNormal00);
   tolua_function(tolua_S,"getTexturePressed",tolua_XXUI_XXButton_getTexturePressed00);
   tolua_function(tolua_S,"getTextureDisabled",tolua_XXUI_XXButton_getTextureDisabled00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXCheckBox","XXCheckBox","CheckBox",NULL);
  tolua_beginmodule(tolua_S,"XXCheckBox");
   tolua_function(tolua_S,"create",tolua_XXUI_XXCheckBox_create00);
   tolua_function(tolua_S,"getTextureBackGround",tolua_XXUI_XXCheckBox_getTextureBackGround00);
   tolua_function(tolua_S,"getTextureBackGroundSelected",tolua_XXUI_XXCheckBox_getTextureBackGroundSelected00);
   tolua_function(tolua_S,"getTextureFrontCross",tolua_XXUI_XXCheckBox_getTextureFrontCross00);
   tolua_function(tolua_S,"getTextureBackGroundDisabled",tolua_XXUI_XXCheckBox_getTextureBackGroundDisabled00);
   tolua_function(tolua_S,"getTextureFrontCrossDisabled",tolua_XXUI_XXCheckBox_getTextureFrontCrossDisabled00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXSlider","XXSlider","Slider",NULL);
  tolua_beginmodule(tolua_S,"XXSlider");
   tolua_function(tolua_S,"create",tolua_XXUI_XXSlider_create00);
   tolua_function(tolua_S,"getTexture",tolua_XXUI_XXSlider_getTexture00);
   tolua_function(tolua_S,"getProgressBarTexture",tolua_XXUI_XXSlider_getProgressBarTexture00);
   tolua_function(tolua_S,"getSlidBallNormalTexture",tolua_XXUI_XXSlider_getSlidBallNormalTexture00);
   tolua_function(tolua_S,"getSlidBallPressedTexture",tolua_XXUI_XXSlider_getSlidBallPressedTexture00);
   tolua_function(tolua_S,"getSlidBallDisabledTexture",tolua_XXUI_XXSlider_getSlidBallDisabledTexture00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXLabelAtlas","XXLabelAtlas","LabelAtlas",NULL);
  tolua_beginmodule(tolua_S,"XXLabelAtlas");
   tolua_function(tolua_S,"create",tolua_XXUI_XXLabelAtlas_create00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXLabel","XXLabel","Label",NULL);
  tolua_beginmodule(tolua_S,"XXLabel");
   tolua_function(tolua_S,"create",tolua_XXUI_XXLabel_create00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXLayout","XXLayout","Layout",NULL);
  tolua_beginmodule(tolua_S,"XXLayout");
   tolua_function(tolua_S,"getBackGroundImage",tolua_XXUI_XXLayout_getBackGroundImage00);
   tolua_function(tolua_S,"create",tolua_XXUI_XXLayout_create00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXWidget","XXWidget","Widget",NULL);
  tolua_beginmodule(tolua_S,"XXWidget");
   tolua_function(tolua_S,"create",tolua_XXUI_XXWidget_create00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXTextField","XXTextField","TextField",NULL);
  tolua_beginmodule(tolua_S,"XXTextField");
   tolua_function(tolua_S,"create",tolua_XXUI_XXTextField_create00);
   tolua_function(tolua_S,"getTextHorizontalAlignment",tolua_XXUI_XXTextField_getTextHorizontalAlignment00);
   tolua_function(tolua_S,"getTextVerticalAlignment",tolua_XXUI_XXTextField_getTextVerticalAlignment00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXScrollView","XXScrollView","ScrollView",NULL);
  tolua_beginmodule(tolua_S,"XXScrollView");
   tolua_function(tolua_S,"getBackGroundImage",tolua_XXUI_XXScrollView_getBackGroundImage00);
   tolua_function(tolua_S,"create",tolua_XXUI_XXScrollView_create00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXListView","XXListView","ListView",NULL);
  tolua_beginmodule(tolua_S,"XXListView");
   tolua_function(tolua_S,"getBackGroundImage",tolua_XXUI_XXListView_getBackGroundImage00);
   tolua_function(tolua_S,"create",tolua_XXUI_XXListView_create00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"XXPageView","XXPageView","PageView",NULL);
  tolua_beginmodule(tolua_S,"XXPageView");
   tolua_function(tolua_S,"getBackGroundImage",tolua_XXUI_XXPageView_getBackGroundImage00);
   tolua_function(tolua_S,"create",tolua_XXUI_XXPageView_create00);
  tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}


#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 501
 TOLUA_API int luaopen_XXUI (lua_State* tolua_S) {
 return tolua_XXUI_open(tolua_S);
};
#endif

