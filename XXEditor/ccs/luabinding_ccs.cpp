/*
** Lua binding: all_ccs
** Generated automatically by tolua++-1.0.92 on 09/10/15 19:30:31.
*/

/****************************************************************************
 Copyright (c) 2011 cocos2d-x.org

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

extern "C" {
#include "tolua_fix.h"
}

#include <map>
#include <string>
#include "cocos2d.h"
#include "CCLuaEngine.h"
#include "SimpleAudioEngine.h"
#include "cocos-ext.h"

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

/* Exported function */
TOLUA_API int  tolua_all_ccs_open (lua_State* tolua_S);

#include "BoxList.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"TreeItem");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(TreeItem)), "TreeItem");
 tolua_usertype(tolua_S,"Widget");
 toluafix_add_type_mapping(CLASS_HASH_CODE(typeid(Widget)), "Widget");
}

/* method: getWidget of class  TreeItem */
#ifndef TOLUA_DISABLE_tolua_all_ccs_TreeItem_getWidget00
static int tolua_all_ccs_TreeItem_getWidget00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"TreeItem",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  TreeItem* self = (TreeItem*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getWidget'", NULL);
#endif
  {
   Widget* tolua_ret = (Widget*)  self->getWidget();
    int nID = (tolua_ret) ? (int)tolua_ret->m_uID : -1;
    int* pLuaID = (tolua_ret) ? &tolua_ret->m_nLuaID : NULL;
    toluafix_pushusertype_ccobject(tolua_S, nID, pLuaID, (void*)tolua_ret,"Widget");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getWidget'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: setWidget of class  TreeItem */
#ifndef TOLUA_DISABLE_tolua_all_ccs_TreeItem_setWidget00
static int tolua_all_ccs_TreeItem_setWidget00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"TreeItem",0,&tolua_err) ||
     !tolua_isusertype(tolua_S,2,"Widget",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  TreeItem* self = (TreeItem*)  tolua_tousertype(tolua_S,1,0);
  Widget* w = ((Widget*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setWidget'", NULL);
#endif
  {
   self->setWidget(w);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setWidget'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: create of class  TreeItem */
#ifndef TOLUA_DISABLE_tolua_all_ccs_TreeItem_create00
static int tolua_all_ccs_TreeItem_create00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"TreeItem",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   TreeItem* tolua_ret = (TreeItem*)  TreeItem::create();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"TreeItem");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: addChild of class  TreeItem */
#ifndef TOLUA_DISABLE_tolua_all_ccs_TreeItem_addChild00
static int tolua_all_ccs_TreeItem_addChild00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"TreeItem",0,&tolua_err) ||
     !tolua_isusertype(tolua_S,2,"TreeItem",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  TreeItem* self = (TreeItem*)  tolua_tousertype(tolua_S,1,0);
  TreeItem* child = ((TreeItem*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'addChild'", NULL);
#endif
  {
   self->addChild(child);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'addChild'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: setText of class  TreeItem */
#ifndef TOLUA_DISABLE_tolua_all_ccs_TreeItem_setText00
static int tolua_all_ccs_TreeItem_setText00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"TreeItem",0,&tolua_err) ||
     !tolua_isstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  TreeItem* self = (TreeItem*)  tolua_tousertype(tolua_S,1,0);
  const char* text = ((const char*)  tolua_tostring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setText'", NULL);
#endif
  {
   self->setText(text);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setText'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: setChecked of class  TreeItem */
#ifndef TOLUA_DISABLE_tolua_all_ccs_TreeItem_setChecked00
static int tolua_all_ccs_TreeItem_setChecked00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"TreeItem",0,&tolua_err) ||
     !tolua_isboolean(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  TreeItem* self = (TreeItem*)  tolua_tousertype(tolua_S,1,0);
  bool checked = ((bool)  tolua_toboolean(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setChecked'", NULL);
#endif
  {
   self->setChecked(checked);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setChecked'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* get function: parent of class  TreeItem */
#ifndef TOLUA_DISABLE_tolua_get_TreeItem_parent_ptr
static int tolua_get_TreeItem_parent_ptr(lua_State* tolua_S)
{
  TreeItem* self = (TreeItem*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'parent'",NULL);
#endif
   tolua_pushusertype(tolua_S,(void*)self->parent,"TreeItem");
 return 1;
}
#endif //#ifndef TOLUA_DISABLE

/* set function: parent of class  TreeItem */
#ifndef TOLUA_DISABLE_tolua_set_TreeItem_parent_ptr
static int tolua_set_TreeItem_parent_ptr(lua_State* tolua_S)
{
  TreeItem* self = (TreeItem*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  tolua_Error tolua_err;
  if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'parent'",NULL);
  if (!tolua_isusertype(tolua_S,2,"TreeItem",0,&tolua_err))
   tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->parent = ((TreeItem*)  tolua_tousertype(tolua_S,2,0))
;
 return 0;
}
#endif //#ifndef TOLUA_DISABLE

/* method: removeAllChildren of class  TreeItem */
#ifndef TOLUA_DISABLE_tolua_all_ccs_TreeItem_removeAllChildren00
static int tolua_all_ccs_TreeItem_removeAllChildren00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"TreeItem",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  TreeItem* self = (TreeItem*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'removeAllChildren'", NULL);
#endif
  {
   self->removeAllChildren();
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'removeAllChildren'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: childrenCount of class  TreeItem */
#ifndef TOLUA_DISABLE_tolua_all_ccs_TreeItem_childrenCount00
static int tolua_all_ccs_TreeItem_childrenCount00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"TreeItem",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  TreeItem* self = (TreeItem*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'childrenCount'", NULL);
#endif
  {
   int tolua_ret = (int)  self->childrenCount();
   tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'childrenCount'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: childrenAtIndex of class  TreeItem */
#ifndef TOLUA_DISABLE_tolua_all_ccs_TreeItem_childrenAtIndex00
static int tolua_all_ccs_TreeItem_childrenAtIndex00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"TreeItem",0,&tolua_err) ||
     !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  TreeItem* self = (TreeItem*)  tolua_tousertype(tolua_S,1,0);
  int i = ((int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'childrenAtIndex'", NULL);
#endif
  {
   TreeItem* tolua_ret = (TreeItem*)  self->childrenAtIndex(i);
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"TreeItem");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'childrenAtIndex'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* Open function */
TOLUA_API int tolua_all_ccs_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
  tolua_cclass(tolua_S,"TreeItem","TreeItem","",NULL);
  tolua_beginmodule(tolua_S,"TreeItem");
   tolua_function(tolua_S,"getWidget",tolua_all_ccs_TreeItem_getWidget00);
   tolua_function(tolua_S,"setWidget",tolua_all_ccs_TreeItem_setWidget00);
   tolua_function(tolua_S,"create",tolua_all_ccs_TreeItem_create00);
   tolua_function(tolua_S,"addChild",tolua_all_ccs_TreeItem_addChild00);
   tolua_function(tolua_S,"setText",tolua_all_ccs_TreeItem_setText00);
   tolua_function(tolua_S,"setChecked",tolua_all_ccs_TreeItem_setChecked00);
   tolua_variable(tolua_S,"parent",tolua_get_TreeItem_parent_ptr,tolua_set_TreeItem_parent_ptr);
   tolua_function(tolua_S,"removeAllChildren",tolua_all_ccs_TreeItem_removeAllChildren00);
   tolua_function(tolua_S,"childrenCount",tolua_all_ccs_TreeItem_childrenCount00);
   tolua_function(tolua_S,"childrenAtIndex",tolua_all_ccs_TreeItem_childrenAtIndex00);
  tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}


#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 501
 TOLUA_API int luaopen_all_ccs (lua_State* tolua_S) {
 return tolua_all_ccs_open(tolua_S);
};
#endif

