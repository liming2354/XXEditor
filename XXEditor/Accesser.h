#ifndef _ATTRIBUTE_H_
#define _ATTRIBUTE_H_

#include "qstring.h"
#include "BoxList.h"

class Accesser
{
public:
    static QString get(Widget* item, const QString& name);
    static void set(Widget* widget, const QString& name, const QString& value);

    static QString toString(const QList<TreeItem*>& lst);
    static QString getCode(const QList<TreeItem*>& lst);
    static QString getCodeCpp(const QList<TreeItem*>& lst);
    static QList<TreeItem*> fromString(const QString& str);
    static QList<TreeItem*> fromFile(const QString& filePath);
};

#endif // !_ATTRIBUTE_H_
