#ifndef _EDITOR_SCENE_H_
#define _EDITOR_SCENE_H_

#include "layers_scenes_transitions_nodes/CCScene.h"
#include "layers_scenes_transitions_nodes/CCLayer.h"
#include "BaseClasses/UIWidget.h"
#include "GLWidget.h"
#include "qtreewidget.h"

using namespace cocos2d;
using namespace ui;

class TreeItem;
class BoxNode;

class EditorScene : public QCGLWidget
{
    Q_OBJECT
    typedef QCGLWidget Super;
public:
    EditorScene(QWidget* parent);

    bool init(const QString&);

    Widget* getRoot() { return _root; }
    Widget* getTouched(const CCPoint& pos);
    Widget* doSelectedPos(const CCPoint& pos);
    
    void selectedWidget(Widget* wid);

    CCPoint convertToGL(const QPoint& pos);

public slots:
    void doSelected(TreeItem* item);
    void aliginX();
    void aliginY();
    void aliginH();
    void aliginV();

    void zoomIn();
    void zoomOut();

    void setBGColor(const QColor&);

signals:
    void currentPropChange(const QString&);

protected:
    virtual void mousePressEvent(QMouseEvent* event);
    virtual void mouseMoveEvent(QMouseEvent* event);
    virtual void mouseReleaseEvent(QMouseEvent* event);
    virtual void resizeEvent(QResizeEvent* event);

    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);

private:
    void dragStart();
    void dragEnd();

    void setCurrent(Widget* w);
    bool checkTouch(Widget* w, const CCPoint& pos);
    Widget* getTouched(Widget* root, const CCPoint& pos);
    int getMouseState(const CCPoint& pos);
    void updateArea(float left, float top, float right, float bottom);
    void updateArea(float pixelDeltaX, float pixelDeltaY);

    CCPoint _lastPos;
    CCPoint _startPos;

    CCScene* _scene;
    Widget* _current;
    BoxNode* _box;
    Widget* _root;
    CCLayerColor* _colorbg;

    int _mouseState;
    
    bool _moveRoot;
    std::map<int, int> _keyPressMap;

    std::map<Widget*, std::pair<CCPoint, CCPoint>> _dragStart;
};

#endif//_EDITOR_SCENE_H_