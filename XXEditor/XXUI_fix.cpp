#include "XXUI_fix.h"
#include "Options.h"

static void draw_bdbox(Widget* w)
{
    const CCSize& size = w->getSize();
    const CCPoint& anchor = w->getAnchorPoint();

    CCPoint origin(-size.width * anchor.x, -size.height * anchor.y);
    CCPoint destination(origin + size);

    ccDrawColor4F(70.0f / 255.0f, 124.0f / 255.0f, 212.0f / 255.0f, 1);
    ccDrawRect(origin, destination);
}

#define draw_layout() \
if (_backGroundImage && _backGroundScale9Enabled)\
{\
    Layout::draw();\
    return;\
}\
    if (Options::current.boxVisible) \
    draw_bdbox(this)

#define draw_widget(type) \
if (Options::current.boxVisible && !_ignoreSize)\
draw_bdbox(this);\
    type::draw()

void XXLayout::draw()
{
    draw_layout();
}

void XXScrollView::draw()
{
    draw_layout();
}

void XXListView::draw()
{
    draw_layout();
}

void XXPageView::draw()
{
    draw_layout();
}

void XXLabel::draw()
{
    draw_widget(Label);
}

void XXWidget::draw()
{
    if (!Options::current.boxVisible) return;

    if (!_ignoreSize)
    {
        draw_bdbox(this);
    }
    else
    {
        ccDrawColor4F(70.0f / 255.0f, 124.0f / 255.0f, 212.0f / 255.0f, 1);
        ccDrawPoint(ccp(0, 0));
    }
}

void XXTextField::draw()
{
    draw_widget(TextField);
}

void XXLabelAtlas::draw()
{
    draw_widget(LabelAtlas);
}

void XXSlider::draw()
{
    draw_widget(Slider);
}

void XXCheckBox::draw()
{
    draw_widget(CheckBox);

}

void XXButton::draw()
{

    draw_widget(Button);
}

void XXLabelBMFont::draw()
{
    draw_widget(LabelBMFont);

}

void XXLoadingBar::draw()
{
    draw_widget(LoadingBar);

}

void XXImageView::draw()
{
    draw_widget(ImageView);

}
