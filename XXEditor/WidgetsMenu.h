#ifndef __XX_MENU_H__
#define __XX_MENU_H__

#include "qmenu.h"

class widgetsMenu : public QMenu
{
    Q_OBJECT

public:

    enum cfg_key
    {
        kDelete = 1,
        kCopy = 2,
        kPaste = 4,
        kAddFrom = 8,
        kSave = 16,
        kCode = 32,
        kCodeCpp = 64,
    };

    widgetsMenu(QWidget* parent);

    static widgetsMenu* create(QWidget* parent);

    widgetsMenu* cfg(cfg_key, bool d);

    void showAtPos();

signals:
    void onAdd(const QString& type);
    void onTouch(int k);

private slots:
    void onAddAction(QAction*);
    void onActionTriggered(QAction*);

private:
    void setup();

    int _cfg;
};

#endif//__XX_MENU_H__